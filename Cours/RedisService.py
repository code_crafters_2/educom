import redis
from time import time
from Cours.models import User, Role, Cours, Niveau


class RedisService:
    def __init__(self, host, port, db):
        self.redis_conn = redis.StrictRedis(host=host, port=port, db=db)
        self.COURS_COUNTER_KEY = 'cours_id_counter'
        self.COURS_PREFIX = 'cours:'
        self.USER_PREFIX = 'user:'
        self.SUBSCRIPTION_PREFIX = 'subscription:'
        self.ROLE_PREFIX = 'role:'
        self.NIVEAU_PREFIX = 'niveau:'

    # User
    def add_user(self, user: User):
        user_key = f'{self.USER_PREFIX}{user.id}'
        user_data = {
            'nom': user.nom,
            'prénom': user.prénom,
            'email': user.email,
            'password': user.password,
            'role_id': user.role.value,
        }
        self.redis_conn.hmset(user_key, user_data)
        return user.id

    def get_user_by_id(self, user_id):
        return self.redis_conn.hgetall(f'{self.USER_PREFIX}{user_id}')

    # Role
    def add_role(self, role: Role):
        role_key = f'{self.ROLE_PREFIX}{role.value}'
        role_data = {
            'name': role.name,
        }
        self.redis_conn.hmset(role_key, role_data)

    def get_role_by_id(self, role_id):
        return self.redis_conn.hgetall(f'{self.ROLE_PREFIX}{role_id}')

    # Niveau
    def add_niveau(self, niveau: Niveau):
        niveau_key = f'{self.NIVEAU_PREFIX}{niveau.value}'
        niveau_data = {
            'name': niveau.name,
        }
        self.redis_conn.hmset(niveau_key, niveau_data)

    def get_niveau_by_id(self, niveau_id):
        return self.redis_conn.hgetall(f'{self.NIVEAU_PREFIX}{niveau_id}')

    # Cours
    def add_cours(self, cours: Cours):
        cours_key = f'{self.COURS_PREFIX}{cours.id}'

        cours_data = {
            'titre': cours.titre,
            'resume': cours.resumer,
            'niveau_id': cours.niveau.value,
            'archive': 'False',
            'professeur_id': cours.professeur.id,
        }
        self.redis_conn.hmset(cours_key, cours_data)
        self.set_cours_timestamp(cours.id)
        return cours.id

    def get_cours_by_id(self, cours_id):
        return self.redis_conn.hgetall(f'{self.COURS_PREFIX}{cours_id}')

    # Subscription
    def subscribe_cours(self, user_id, cours_id):
        self.redis_conn.sadd(f'{self.SUBSCRIPTION_PREFIX}{user_id}:subscriptions', cours_id)

    def unsubscribe_cours(self, user_id, cours_id):
        self.redis_conn.srem(f'{self.SUBSCRIPTION_PREFIX}{user_id}:subscriptions', cours_id)

    def get_user_subscriptions(self, user_id):
        return self.redis_conn.smembers(f'{self.SUBSCRIPTION_PREFIX}{user_id}:subscriptions')

    # Méthodes utilitaires
    def get_or_add_user(self, user: User):
        user_key = f'{self.USER_PREFIX}{user.id}'
        if not self.redis_conn.exists(user_key):
            self.add_user(user)
        return user.id
    
    def set_cours_timestamp(self, cours_id):
        timestamp = int(time())
        self.redis_conn.hset(f'cours:{cours_id}', 'timestamp', timestamp)
        self.redis_conn.expire(f'cours:{cours_id}', 604800)

    def get_user_subscribed_cours(self, user_id):
        subscription_key = f'{self.SUBSCRIPTION_PREFIX}{user_id}:subscriptions'
        subscribed_cours_ids = self.redis_conn.smembers(subscription_key)
        subscribed_cours = []
        for cours_id in subscribed_cours_ids:
            cours_data = self.redis_conn.hgetall(f'{self.COURS_PREFIX}{cours_id}')
            cours_data[b'id'] = cours_id
            subscribed_cours.append(cours_data)
        return subscribed_cours

    def get_cours_by_query(self, **kwargs):
        # Construire la clé de recherche en fonction des arguments de la requête
        search_key = ":".join([f"{key}={value}" for key, value in kwargs.items()])

        # Vérifier si les données existent dans Redis
        cours_data = self.redis_conn.hgetall(search_key)
        if cours_data:
            return cours_data
        return []
