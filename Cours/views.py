from django.contrib import messages
from django.db.models import Case, When, Value, BooleanField
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.conf import settings
from Cours.models import Cours, Role, Suscription
from Cours.RedisService import RedisService
from Educom.forms import CoursForm

# Initialisez votre service RedisService avec les paramètres de connexion à Redis
redis_service = RedisService(settings.REDIS_HOST, settings.REDIS_PORT, settings.REDIS_DB)


def dashboard_view(request):
    if 'user_id' in request.COOKIES and 'user_role' in request.COOKIES:
        if request.COOKIES['user_role'] == Role.ETUDIANT.value:
            query = request.GET.get('q')
            if query:
                cours = get_cours_from_redis_or_db(titre__icontains=query, archive=False)
            else:
                cours = get_cours_from_redis_or_db(archive=False)

            # Récupérer les cours auxquels l'utilisateur est abonné depuis Redis
            user_subscribed_cours = redis_service.get_user_subscribed_cours(request.COOKIES.get('user_id'))

            # Si l'utilisateur n'est abonné à aucun cours, récupérer les cours de la base de données
            if not user_subscribed_cours:
                user_subscribed_cours = Suscription.objects.filter(user_id=request.COOKIES.get('user_id'))
                subscribed_map = {subscription.cours.id: subscription for subscription in user_subscribed_cours}
            else:
                # Formater les cours récupérés de Redis
                subscribed_map = {int(cours[b'id']): cours for cours in user_subscribed_cours}

            context = {
                'courses': cours,
                'subscribed_map': subscribed_map,
                'notifications': [
                    {'titre': 'Titre du cours 1', 'date': '2024-04-30', 'creation': True},
                    {'titre': 'Titre du cours 2', 'date': '2024-05-02', 'creation': False}
                ]
            }
            return render(request, 'Cours/templates/dashboard_etudiant.html', context=context)

        elif request.COOKIES['user_role'] == Role.PROFESSEUR.value:
            query = request.GET.get('q')
            if query:
                cours = get_cours_from_redis_or_db(titre__icontains=query, professeur_id=request.COOKIES.get('user_id'))
            else:
                cours = get_cours_from_redis_or_db(professeur_id=request.COOKIES.get('user_id'))

            context = {
                'courses': cours,
                'recherche': request.GET.get('q'),
            }
            return render(request, 'Cours/templates/dashboard_enseignant.html', context=context)
    return redirect(reverse('login'))


def get_cours_from_redis_or_db(**kwargs):
    cours_data = redis_service.get_cours_by_query(**kwargs)
    if cours_data:
        return cours_data
    cours_queryset = Cours.objects.filter(**kwargs).values()
    cours_data = list(cours_queryset)
    for cours in cours_data:
        redis_service.add_cours(Cours(**cours))
    return cours_data

def all_courses_view(request):
    if 'user_id' in request.COOKIES and 'user_role' in request.COOKIES:
        query = request.GET.get('q')
        subscribed = request.GET.get('subscribed')
        unsubscribed = request.GET.get('unsubscribed')
        
        # Récupérer les cours auxquels l'utilisateur est abonné depuis Redis
        user_subscriptions = redis_service.get_user_subscriptions(request.COOKIES.get('user_id'))
        
        # Créer une expression conditionnelle pour trier les cours
        subscribed_case = Case(
            When(id__in=user_subscriptions, then=Value(0)),
            default=Value(1),
            output_field=BooleanField()
        )
        
        if query:
            if subscribed and not unsubscribed:
                cours_query = Cours.objects.filter(titre__icontains=query, id__in=user_subscriptions, archive=False)
            elif unsubscribed and not subscribed:
                cours_query = Cours.objects.filter(titre__icontains=query, archive=False).exclude(id__in=user_subscriptions)
            else:
                cours_query = Cours.objects.filter(titre__icontains=query, archive=False)
        else:
            if subscribed and not unsubscribed:
                cours_query = Cours.objects.filter(id__in=user_subscriptions, archive=False)
            elif unsubscribed and not subscribed:
                cours_query = Cours.objects.exclude(id__in=user_subscriptions).filter(archive=False)
            else:
                cours_query = Cours.objects.filter(archive=False)
        
        # Annoter les cours avec l'expression conditionnelle
        cours_query = cours_query.annotate(subscribed=subscribed_case)
        
        # Récupérer les cours sous forme de liste
        cours = list(cours_query.order_by('subscribed', 'titre').values())
        
        # Récupérer les détails complets des cours pour construire subscribed_map
        cours_details = {cours.id: cours for cours in Cours.objects.filter(id__in=user_subscriptions)}
        
        context = {
            'courses': cours,
            'subscribed_map': cours_details,
            'recherche': {
                'query': query,
                'subscribed': subscribed,
                'unsubscribed': unsubscribed
            }
        }
        return render(request, 'all_courses.html', context=context)
    return redirect(reverse('login'))

def create_cours_view(request, cours_id=None):
    if 'user_id' in request.COOKIES and 'user_role' in request.COOKIES and request.COOKIES['user_role'] == Role.PROFESSEUR.value:
        if request.method == 'POST':
            form = CoursForm(request.POST)
            if form.is_valid():
                if cours_id:
                    cours = Cours.objects.get(pk=cours_id)
                    cours.titre = form.cleaned_data['titre']
                    cours.resumer = form.cleaned_data['resumer']
                    cours.niveau = form.cleaned_data['niveau']
                    cours.professeur_id = int(request.COOKIES.get('user_id'))
                    cours.archive = False
                    cours.save()
                else:
                    cours = form.save(commit = False)
                    cours.professeur_id = int(request.COOKIES.get('user_id'))
                    cours.save()
                redis_service.add_cours(cours)
                return redirect(reverse('dashboard'))  # Rediriger vers le tableau de bord après la création du cours
        else:
            if cours_id:
                all_cours = Cours.objects.get(id = cours_id)
                initial_data = {'titre': all_cours.titre, 'resumer': all_cours.resumer, 'niveau': all_cours.niveau}
            else:
                initial_data = None
            form = CoursForm(initial=initial_data)
        return render(request, 'create_cours.html', {'form': form})

def cours_details_view(request, cours_id):
    if 'user_id' in request.COOKIES and 'user_role' in request.COOKIES:
        # Récupérer le cours spécifique en fonction de l'ID fourni
        cours = get_object_or_404(Cours, id=cours_id)
        # Vérifier s'il est abonné à ce cours 
        cours_subscriptions = Suscription.objects.filter(user_id=request.COOKIES.get('user_id'), cours_id=cours_id)
        isSubscribe = True if cours_subscriptions else False
        # Vérifier s'il est prof
        isProf = cours.professeur_id == int(request.COOKIES.get('user_id'))
        return render(request, 'cours_details.html', {'cours': cours, 'isProf': isProf, 'isSubscribe': isSubscribe})



def subscribe_cours_view(request, cours_id):
    if 'user_id' in request.COOKIES and 'user_role' in request.COOKIES and request.COOKIES['user_role'] == Role.ETUDIANT.value:
        # Créer une inscription pour l'utilisateur actuel et le cours spécifique
        Suscription.objects.create(user_id=request.COOKIES.get('user_id'), cours_id=cours_id)
        
        # Ajouter l'utilisateur à l'abonnement du cours dans Redis
        redis_service.subscribe_cours(user_id=request.COOKIES.get('user_id'), cours_id=cours_id)
        
        messages.success(request, f"Vous vous êtes abonné au cours.")
        return redirect('dashboard')

def unsubscribe_cours_view(request, cours_id):
    if 'user_id' in request.COOKIES and 'user_role' in request.COOKIES and request.COOKIES['user_role'] == Role.ETUDIANT.value:
        # Récupérer l'inscription spécifique de l'utilisateur actuel pour le cours spécifique
        subscription = get_object_or_404(Suscription, user=request.COOKIES.get('user_id'), cours_id=cours_id)
        
        # Supprimer l'inscription de la base de données
        subscription.delete()
        
        # Désabonner l'utilisateur du cours dans Redis
        redis_service.unsubscribe_cours(user_id=request.COOKIES.get('user_id'), cours_id=cours_id)
        
        cours = subscription.cours
        messages.success(request, f"Vous vous êtes désabonné du cours {cours.titre}.")
        return redirect('dashboard')

    
def archive_cours(request, cours_id):
    if 'user_id' in request.COOKIES and 'user_role' in request.COOKIES and request.COOKIES['user_role'] == Role.PROFESSEUR.value:
        # Récupérer le cours associé à l'id
        cours = Cours.objects.get(id=cours_id)
        if cours.professeur_id == int(request.COOKIES.get('user_id')):
            cours.archive = True
            cours.save()
            messages.success(request, f"Vous avez archivé le cours {cours.titre}.")
        return redirect('dashboard')
