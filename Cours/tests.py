from django.test import TestCase

from django.test import TestCase
from Cours.models import User, Role, Cours, Niveau, Suscription
from Cours.RedisService import RedisService

class RedisServiceTestCase(TestCase):
    def setUp(self):
        self.redis_service = RedisService(host='localhost', port=6379, db=0)

    def test_user_methods(self):
        user = User.objects.create(nom='John', prénom='Doe', email='john@example.com', password='1234', role=Role.ETUDIANT)
        user_id = self.redis_service.add_user(user)
        self.assertIsNotNone(user_id)

        redis_user = self.redis_service.get_user_by_id(user_id)
        self.assertEqual(redis_user[b'nom'], b'John')

    def test_role_methods(self):
        role = Role.ETUDIANT
        self.redis_service.add_role(role)
        redis_role = self.redis_service.get_role_by_id(role.value)
        self.assertEqual(redis_role[b'name'], b'ETUDIANT')

    def test_niveau_methods(self):
        niveau = Niveau.DEBUTANT
        self.redis_service.add_niveau(niveau)
        redis_niveau = self.redis_service.get_niveau_by_id(niveau.value)
        self.assertEqual(redis_niveau[b'name'], b'DEBUTANT')

    def test_cours_methods(self):
        professeur = User.objects.create(nom='Jane', prénom='Doe', email='jane@example.com', password='5678', role=Role.PROFESSEUR)
        cours = Cours(titre='Maths', resume='Cours de mathématiques', niveau=Niveau.AVANCE, professeur=professeur)
        cours_id = self.redis_service.add_cours(cours)
        self.assertIsNotNone(cours_id)

        redis_cours = self.redis_service.get_cours_by_id(cours_id)
        self.assertEqual(redis_cours[b'titre'], b'Maths')


    def test_subscription_methods(self):
        user = User.objects.create(nom='Alice', prénom='Doe', email='alice@example.com', password='abcd', role=Role.ETUDIANT)
        cours_id = 1  # ID d'un cours fictif pour le test
        self.redis_service.subscribe_cours(user_id=user.id, cours_id=cours_id)
        subscriptions = self.redis_service.get_user_subscriptions(user_id=user.id)
        self.assertIn(cours_id.encode(), subscriptions)

    def test_subscription_methods(self):
        user = User.objects.create(nom='Alice', prénom='Doe', email='alice@example.com', password='abcd', role=Role.ETUDIANT)
        cours_id = 1  # ID d'un cours fictif pour le test
        self.redis_service.subscribe_cours(user_id=user.id, cours_id=cours_id)
        subscriptions = self.redis_service.get_user_subscriptions(user_id=user.id)
        # Convertir cours_id en chaîne de caractères avant l'assertion
        cours_id_str = str(cours_id)
        self.assertIn(cours_id_str.encode(), subscriptions)