from django.contrib import admin

from Cours.models import Cours, Suscription, User

# Register your models here.
admin.site.register(User)
admin.site.register(Cours)
admin.site.register(Suscription)