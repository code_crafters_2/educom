from django.db import models
from enumfields import EnumField
from enum import Enum

class Role(Enum):
    ETUDIANT = 'ETUDIANT'
    PROFESSEUR = 'PROFESSEUR'

class Niveau(Enum):
    DEBUTANT = 'DEBUTANT'
    MOYEN = 'MOYEN'
    AVANCE = 'AVANCE'

class User(models.Model):
    nom = models.CharField(max_length=50)
    prénom = models.CharField(max_length=50)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=50)
    role = EnumField(Role, default=Role.ETUDIANT)

    def __str__(self) -> str:
        return self.prénom + ' ' + self.nom
    

class Cours(models.Model):
    titre = models.CharField(max_length=200)
    resumer = models.TextField()
    archive = models.BooleanField(default=False)
    niveau = EnumField(Niveau, default=Niveau.MOYEN)
    professeur = models.ForeignKey(User, related_name='cours_enseignement', on_delete=models.SET_NULL, null=True)

    def __str__(self) -> str:
        return self.titre

class Suscription(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cours = models.ForeignKey(Cours, on_delete=models.CASCADE)
    nouveau = models.BooleanField(default=True)

    def __str__(self) -> str:
        return self.user.nom + " - " + self.cours.titre
