from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('', views.dashboard_view, name='dashboard'),
    path('all-courses/', views.all_courses_view, name='all_courses'),
    path('create/', views.create_cours_view, name='create_cours'),
    path('create/<int:cours_id>/', views.create_cours_view, name='create_cours'),
    path('<int:cours_id>/', views.cours_details_view, name='cours_details'),
    path('subscribe/<int:cours_id>/', views.subscribe_cours_view, name='subscribe_cours'),
    path('unsubscribe/<int:cours_id>/', views.unsubscribe_cours_view, name='unsubscribe_cours'),
    path('archive/<int:cours_id>/', views.archive_cours, name='archive_cours'),
]