from django import forms
from Cours.models import Cours, User

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['nom', 'prénom', 'email', 'password', 'role']
        widgets = {
            'password': forms.PasswordInput(),  # Utilisez PasswordInput pour masquer le champ du mot de passe
        }

class LoginForm(forms.Form):
    email = forms.CharField(label='Email', max_length=100)
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

class CoursForm(forms.ModelForm):
    class Meta:
        model = Cours
        fields = ['titre', 'resumer', 'niveau']

