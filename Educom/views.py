from django.shortcuts import redirect, render
from django.urls import reverse

from Educom.forms import LoginForm, UserForm
from Cours.models import User
from Educom.utils import set_cookie

def registry_view(request):
    if 'user_id' not in request.COOKIES and 'user_role' not in request.COOKIES:
        if request.method == 'POST':
            user_form = UserForm(request.POST)
            if user_form.is_valid():
                # Vérifier si un utilisateur avec le même email existe déjà
                existing_user = User.objects.filter(email=user_form.cleaned_data['email']).exists()
                if not existing_user:
                    # Créer une instance du modèle User avec les données du formulaire
                    new_user = User(
                        nom=user_form.cleaned_data['nom'],
                        prénom=user_form.cleaned_data['prénom'],
                        email=user_form.cleaned_data['email'],
                        password=user_form.cleaned_data['password'],
                        role=user_form.cleaned_data['role']
                    )
                    # Enregistrer l'utilisateur dans la base de données
                    new_user.save()
                    return redirect(reverse('login'))  # Rediriger vers une vue de confirmation
                else:
                    # Gérer le cas où un utilisateur avec le même email existe déjà
                    # Peut-être afficher un message d'erreur à l'utilisateur
                    pass
        else:
            user_form = UserForm()
        
        context = {
            'user_form': user_form,
        }
        return render(request, 'Educom/templates/registration/registry.html', context=context)
    return redirect(reverse('dashboard'))

def login_view(request):
    if 'user_id' not in request.COOKIES and 'user_role' not in request.COOKIES:
        if request.method == 'POST':
            login_form = LoginForm(request.POST)
            if login_form.is_valid():
                email = login_form.cleaned_data['email']
                password = login_form.cleaned_data['password']
                # Recherchez un utilisateur avec l'email donné
                user = User.objects.filter(email=email).first()
                if user and user.password == password:
                    # Authentification réussie, enregistrez l'utilisateur dans la session
                    response = redirect(reverse('dashboard'))  # Rediriger vers une vue de confirmation
                    set_cookie(response, 'user_id', user.id)
                    set_cookie(response, 'user_role', user.role.value)
                    return response
                else:
                    # Gérer le cas où l'authentification a échoué
                    pass
        else:
            login_form = LoginForm()
        
        context = {
            'login_form': login_form,
        }
        return render(request, 'Educom/templates/registration/login.html', context=context)
    return redirect(reverse('dashboard'))

def logout_view(request):
    response = redirect(reverse('login'))
    response.delete_cookie('user_id', path='/')
    response.delete_cookie('user_role', path='/')

    return response
