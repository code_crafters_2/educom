# Educom

## Pour les developpeurs

- Récupération du projet

```sh
git clone https://gitlab.com/code_crafters_2/educom.git
cd educom
py -m venv my_env
.\my_env\Scripts\activate
pip install -r requirements.txt
```

- Installation Redis sur WSL (windows)

```sh
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list
sudo apt-get update
sudo apt-get install redis
```

- Lancer le serveur Redis sur WSL (Windows)

```sh
sudo service redis-server start
```

- Lancer le projet en local

```sh
py manage.py migrate
py manage.py runserver
```

- Ajouter des modifications

```sh
git checkout main
git pull
git branch {Nom de la branche}
git add /Educom/*
git commit -m "{Message de commit}"
git push
```

- Ajout de dependances pip

```sh
pip install {La dependance}
pip freeze > requirements.txt
```

## Guide

- Les comptes

```sh
Compte Prof :  
   * mail = marcoulnicolas95@gmail.com
   * mdp = 1234

Compte élève :
   * mail = aymeric.chabot@ymail.com
   * mdp = 1234
```

Le jeu de données possèdent 5 cours et 2 abonnements. Afin d'avoir un début de données.
Après cela, vous pouvez vous amuser.

## Description

1. Conception de la base de données Redis :
   - [x] Définir la structure des données pour les cours, les professeurs et les étudiants.
   - [x] Utiliser des structures de données Redis appropriées telles que des hashes, des sets et des listes pour stocker les informations nécessaires.
2. Configuration de l'environnement de développement :
   - [x] Installer Redis et Django.
   - [x] Configurer un environnement virtuel pour votre projet Django.
3. Développement de l'application Django :
   - [x] Créer les modèles Django pour les cours, les professeurs et les étudiants.
   - [x] Implémenter les vues et les templates nécessaires pour afficher les cours, les professeurs et les étudiants.
   - [ ] Mettre en place le système de nouvelles de type publish-subscribe en utilisant Redis pub/sub.
4. Implémentation des fonctionnalités principales :
   - [x] Permettre aux enseignants de publier des mises à jour de cours et de créer de nouveaux cours.
   - [x] Permettre aux étudiants de s'abonner aux mises à jour de cours et de s'inscrire à des cours.
   - [ ] Gérer l'expiration des cours et mettre à jour la date d'expiration lorsque les étudiants s'inscrivent.
5. Ajout de la fonction de recherche :
   - [x] Implémenter une fonction de recherche permettant aux utilisateurs de trouver des cours en fonction de différents critères.
6. Tests et débogage :
   - [x] Tester chaque fonctionnalité pour garantir son bon fonctionnement.
   - [x] Déboguer les erreurs éventuelles.
7. Documentation et soumission :
   - [x] Rédiger une documentation détaillée expliquant comment exécuter l'application, y compris l'installation de Redis, Django et d'autres dépendances.
   - [x] Soumettre le code source de l'application sur Git avec des commits réguliers des deux membres de l'équipe.

## Authors and acknowledgment

[Aymeric CHABOT](https://www.linkedin.com/in/aymeric-chabot-14098718a/)

[Nicolas MARCOUL-LARGORCE](https://www.linkedin.com/in/nicolas-marcoul-lagorce-6b794320a/)
